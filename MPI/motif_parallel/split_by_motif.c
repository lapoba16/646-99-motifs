/*
   Bel LaPointe
   Apr 7, 2016
   CSC 646 - Parallel Programming
   
   Compile
      make
   Run
      Where 'P' is the number of MPI threads to use,
      
      mpiexec -n P ./motifsearch motifFile sequenceFile outputFile
      
      Alternatively, given the ./input/ and ./output/
      directories, the supplied ./defsmall , ./defmed ,
      and ./deflar will run the available inputs with 4,
      4, and 10 threads respectively.
   
   Motifs (Parallel version, distribute sequences)
      Given a series of motifs and sequences, this program
      will count how many sequences contain each motif. This
      version uses MPI to parallelize the tasks by distributing
      the motifs amongst the threads. Each thread receives
      a complete copy of the sequences.
      This program writes updates to stderr as it runs.
*/
#include "include/all.h"

/* countmatches will compare two given char*s
   allocated to fit a 2d array to see if the motifs
   in one exist in the other's sequences, where
   'X' in the motifs is considered a wildcard
   PRE:  each * is already allocated and loaded
         input_ms_cntlen[0]%P == 0 (the number
         of motifs evenly divides by the number
         of threads)
   POST: matchcnt contains the frequency of
         each motif in the entirety of the
         sequence array
*/
bool countmatches(int *input_ms_cntlen, char* mots, char* seqs, int rank, int* matchcnt, int P);

int main(int argc, char** argv)
{
   char *motifs, *sequences;
   int P, my_rank;
   /* [0] = motifs cnt
      [1] = motifs len
      [2] = sequence cnt
      [3] = sequence len
   */
   int ms_cntlen[4], i, j;
   bzero(ms_cntlen, 8*sizeof(int));
   if(argc<4){
      // printf("Usage: mpiexec -n P ./motifsearch motifFile seqFile outputFile\n");
      printf("Usage: ./motifsearch motifFile seqFile outputFile\n");
      return 1;
   }
   MPI_Init(&argc, &argv);
   MPI_Comm_size(MPI_COMM_WORLD, &P);
   MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
   if(my_rank==0){
      if(!read_in(argv[1], argv[2], &motifs, &sequences, ms_cntlen) ){
         fprintf(stderr, "Host %03i : ERR: could not read in\n", my_rank);
         return 2;
      }
      fprintf(stderr, "Host %03i : Read in %i motifs length %i and %i sequences length %i\n", my_rank, ms_cntlen[0], ms_cntlen[1], ms_cntlen[2], ms_cntlen[3]);
   }
   MPI_Barrier(MPI_COMM_WORLD);
   if(my_rank==0)
      tic();
   //    DISTRIBUTE WORK HERE
   MPI_Bcast(ms_cntlen, 8, MPI_INT, 0, MPI_COMM_WORLD);
   int *matchcnt;
   if(my_rank==0)
      matchcnt = (int*)malloc(sizeof(int)*ms_cntlen[0]);
   else
      matchcnt = (int*)malloc(sizeof(int)*ms_cntlen[0]/P);
   bzero(matchcnt, sizeof(int)*(ms_cntlen[0]/P));
   // given new ms_cntlen, allocate space for incoming scatter
   char *my_motifs = (char*)malloc(sizeof(char) * ms_cntlen[0] * (ms_cntlen[1]+1) / P);
   if(my_rank!=0){
      motifs = (char*)malloc(sizeof(char)*1);
      sequences = (char*)malloc(sizeof(char) * ms_cntlen[2] * (ms_cntlen[3]+1) );
   }
   MPI_Scatter(
      motifs, ms_cntlen[0]/P * (ms_cntlen[1]+1), MPI_CHAR, 
      my_motifs, ms_cntlen[0]/P * (ms_cntlen[1]+1), MPI_CHAR, 0, MPI_COMM_WORLD);
   MPI_Bcast(sequences, ms_cntlen[2] * (ms_cntlen[3]+1), MPI_CHAR, 0, MPI_COMM_WORLD);
   //    COUNT THE MATCHES
   countmatches(ms_cntlen, my_motifs, sequences, my_rank, matchcnt, P);
   //    RECONVENE
   int final_matchcnt[ms_cntlen[0]];
   MPI_Gather(
      matchcnt, ms_cntlen[0]/P, MPI_INT,
      final_matchcnt, ms_cntlen[0]/P, MPI_INT, 0, MPI_COMM_WORLD);
   MPI_Barrier(MPI_COMM_WORLD);
   if(my_rank==0){
      toc();
      fprintf(stderr, "Host %03i : Completed in %f seconds (%.1f minutes).\n", my_rank, etime(), (etime()/60));
      if(!print_out(argv[3], motifs, ms_cntlen, final_matchcnt)){
         fprintf(stderr, "Host %03i : ERR: could not print to %s\n", my_rank, argv[3]);
         return 5;
      }
      fprintf(stderr, "Host %03i : Printed to %s\n", my_rank, argv[3]);
   }
   //fprintf(stderr,"%3i : Finished!\n", my_rank);
   free(motifs);
   free(sequences);
   free(matchcnt);
   MPI_Finalize();
   return 0;
}
bool countmatches(int *input_ms_cntlen, char* mots, char* seqs, int rank, int* matchcnt, int P)
{
   char onemotif[100], onesequence[100];
   bool matching = false;
   int i, j, k;
   int motif_cnt = input_ms_cntlen[0] / P;
   int ms_cntlen[4];
   memcpy(ms_cntlen, input_ms_cntlen, sizeof(int)*4);
   for(i=0; i<motif_cnt; i++){
      strcpy( onemotif, getstr(mots, i, ms_cntlen[1]+1));
      if(i>0 && i%250==0)
         fprintf(stderr,"\n   %3i : Checking motif #%7i/%i\n", rank, i, motif_cnt);
      matchcnt[i] = 0;
      for(j=0; j<ms_cntlen[2]; j++){
         strcpy( onesequence, getstr(seqs, j, ms_cntlen[3]+1));
         matching=true;
         for(k=0; k<ms_cntlen[3]; k++)
            if(onemotif[k]!='X' && onemotif[k] != onesequence[k]){
               matching=false;
            }
         matchcnt[i] += matching;
      }
   }
}