#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/fileIO.h"

FILE* manage_file(char* file_name, char* type)
{
   FILE *thisfile = fopen(file_name, type);
   if (!thisfile){
      if(type[0]=='r'){
         fprintf(stderr, "ERR: couldn't read file '%s'.\n", file_name);
         return NULL;
      }
      else if (type[0]=='w'){
         fprintf(stderr, "ERR: couldn't write to file '%s'.\n", file_name);
         return NULL;
      }
      return NULL;
   }
   return thisfile;
}

int main(int argc, char** argv)
{
   int i, j, k;
   char alphabet[] = "ARNDCQEGHILKMFPSTWYV";
   if(argc < 4){
      printf("Usage: ./gen <type> <sample_count> <length_of_sample>\n       where <type> is 'motif' or 'seq'.\n");
      return 1;
   }
   int sample = strtol(argv[2], NULL, 10);
   int length = strtol(argv[3], NULL, 10);
   char filename[100], type[100];
   int odds_of_X = length;
   FILE* output;
   type[0] = 'w';
   type[1] = '\0';
   time_t t;
   srand((unsigned) time(&t));
   // Generate a motif series
   if(argv[1][0]=='m')
      strcpy(filename, "input/my_motif\0");
   else
      strcpy(filename, "input/my_seq\0");
   output = manage_file(filename, type);
   if(output==NULL){
      printf("ERR: count not create %s file\n", filename);
      return 2;
   }
   fprintf(output, "%i %i\n", sample, length);
   
   for( i=0; i<sample; i++){
      odds_of_X = length+1;
      type[0] = '\0';
      for( j=0; j<length; j++){
         if(argv[1][0]=='m' && !(rand()%odds_of_X) ){
            odds_of_X = length+1;
            type[j] = 'X';
         }
         else{
            if (argv[1][0]=='m')
               odds_of_X--;
            type[j] = alphabet[ rand()%20 ];
         }
      }
      type[j] = '\0';
      fprintf(output, "%s\n", type);
   }
   
   fclose(output);
   return 0;
}