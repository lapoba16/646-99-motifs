#include <iostream>
#include <cstdlib>
#include <fstream>
using namespace std;

int main(int argc, char** argv)
{
    ofstream outputFile;
    if(argc<3){
        cout<<"Usage: <name> <N> <M>"<<endl;
        return 1;
    }
    int numSamples = strtol(argv[1], NULL, 10);
    int numElements = strtol(argv[2], NULL, 10);
    //double min = strtod(argv[3], NULL);
    //double max = strtod(argv[4], NULL);
    time_t t;
    
    srand((unsigned) time(&t));
    outputFile.open("inMine", ofstream::out);
    
    outputFile<<numSamples<<" "<<numElements;
    for(int i=0; i<numSamples; i++){
        outputFile<<endl;  
        for(int j=0; j<numElements; j++){
            outputFile<<(float)(rand()%100)/100<<" ";
        } 
    }
    
    outputFile.close();
    return 0;
}