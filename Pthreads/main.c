/*
   Bel LaPointe
   CSC646 - Spr 2016
   March 3, 2016
   
   KCluster Program
      Using PThreads and a file name given as
      a command line argument, generate a given
      number of center points and use the given
      number of threads to cluster the input
      file's points into the center points
      number of groups. Output to the given
      file name the number of points processed
      and each one's group number.
      
   compile: make
       run: ./kcluster K P inputFile outputFile <optional:max_trials>
   Where K=number of centers (groups) and P=number of threads
   
   NOTE: The program prints to stdout the time from
   before the pthread_creates are called to after pthread_join.
   It does NOT include reading or writing to the files.
*/
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include "etime.h"
typedef enum { false, true } bool;

//===========FUNCTIONS================

// Pre: global pointers are delclared and not allocated
// Post: global pointers are now arrays outline in //===GLOBALS===
bool global_mallocs();
// Pre: global pointers are declared and allocated
// Post: global pointers are freed
bool global_frees();
// Pre: type is "w" or "r"
// Post: return FILE* to the given file_name. Exit() if NULL.
FILE* manage_file(char* file_name, char* type);
// Pre: global variables can hold N*M points and K*M centers
// Post: true if **points and **centers full. False if failed.
bool read_in(char* file_name);
// Pre: cur_group[N] is full
// Post: file is created (if not exist) and filled with N, group#, group#....
void print_out(char* file_name);
// Pre: new_centers, cur_centers, cur_centers_cnt are initialized
// Post: cur_centers[i] = new_centers[i] / cur_centers_cnt[i]
//    Changes: cur_centers[K][M], new_centers[K][M], new_centers_cnt[K]
//    Reads:   new_centers[K][M], new_centers_cnt[K]
void realign_centers(int, int, double**, int*);
// Pre: cur_centers and points are initialized
// Post: distance[] and cur_group[] relates to points and cur_centers
//    Changes: cur_group[N], distance[N]
//    Reads:   points[N][M], cur_centers[K][M], cur_group[N]
void realign_groups(int, int, double**, int*);
// Host threaded function. Calls the above.
// Pre: global_malloc is done, points[] and cur_centers[] initialized
// Post: cur_group[] reflects connection to fully kClustered centers.
void* kCluster(void*);
// Pre: prev_group and cur_group are acccurate, only 1 access(sets contineu to false, then true)
// Post: prev_group = cur_group and yes_change is accurate
//    Changes: yes_change, prev_group (<-cur_group)
bool determine_if_continue(int, int);
// Pre: only ONE thread runs this at a time, new_centers and new_centers_cnt are both zeroed out before all threads (not each)
// Post: new_centers and new_centers_cnt now include this thread's findings
void load_local_new_centers(double**, int*);

//===========GLOBALS==================
/*
   cur_centers[K][M]       points[N][M]
   new_centers[K][M]       distance[N]
   new_centers_cnt[K]      cur_gruop[N]
                           prev_group[N]
   K = center count
   N = point count
   M = dimension count
*/
unsigned int K, P, N, M, MAX_TRIALS, global_rounds=0;
double **cur_centers, **points, *distance, **new_centers;
int *cur_group, *prev_group, *new_centers_cnt;
bool yes_change=true, continue_looping=true;
int thrd_cntr=0;
pthread_mutex_t mutex, barrier_one, barrier_two;
pthread_cond_t cond;

//=============MAIN===================

int main(int argc, char** argv)
{
    /*  kmeans K P inputFile outputFile
        argv[0] = a.out
            [1] = K         //# of centers
            [2] = P         //# of threads
            [3] = inputFileName
            [4] = outputFileName
            [5] = MAX_TRIALS (optional, defaults to 100)
    */
   // Command line argument stuff
   if(argc<5){
     printf("Usage: %s #Clusters #Threads inputFileName outputFileName <optional:max_trials>\n", argv[0]);
     return 1;
   }
   time_t t;
   int i;
   K = strtol(argv[1], NULL, 10);
   P = strtol(argv[2], NULL, 10);
   pthread_t *thread_handles = malloc(P * sizeof(pthread_t));
   if(argc>5)
     MAX_TRIALS = strtol(argv[5], NULL, 10);
   else
     MAX_TRIALS = 100;
   srand((unsigned) time(&t));
   
   // Read file
   if(!read_in(argv[3]))
      return 1;
   // points, cur_centers, N, M, K, P, thread_handles are ready
   tic();
   for(i=0; i<P; i++)
      pthread_create(
         &thread_handles[i]
         , NULL
         , kCluster
         , (void*)((long)i)
      );
   //============PARALLEL STUFF=============
   for(i=0; i<P; i++)
      pthread_join( thread_handles[i], NULL);
   toc();
   printf("Parallel time: %lf\n# of Rounds: %i\n", etime(), global_rounds);
   
   // Print to file
   print_out(argv[4]);
   printf("Complete!\n");
   return 0;
}

//=============FUNCTIONS==============

bool global_mallocs()
{
   int i;
   
   cur_centers = (double**)malloc(K * sizeof(double*));
   points = (double**)malloc(N * sizeof(double*));
   new_centers = (double**)malloc(K * sizeof(double*));
   distance = (double*)malloc(N * sizeof(double));
   
   cur_group = (int*)malloc(N * sizeof(int));
   prev_group = (int*)malloc(N * sizeof(int));
   new_centers_cnt = (int*)malloc(K * sizeof(int));
   
   yes_change = true;
   continue_looping = true;
   
   if(K > N)
      for(i=0; i<K; i++){
         if(i<N)
            points[i] = (double*)malloc(M * sizeof(double));
         cur_centers[i] = (double*)malloc(M * sizeof(double));
         new_centers[i] = (double*)malloc(M * sizeof(double));
      }
   else
      for(i=0; i<N; i++){
         if(i<K){
            cur_centers[i] = (double*)malloc(M * sizeof(double));
            new_centers[i] = (double*)malloc(M * sizeof(double));
         }
         points[i] = (double*)malloc(M * sizeof(double));
      }
      
   return true;
}
bool global_frees()
{
   int i;
   
   if(K > N)
      for(i=0; i<K; i++){
         if(i<N)
            free(points[i]);
         free(cur_centers[i]);
         free(new_centers[i]);
      }
   else
      for(i=0; i<N; i++){
         if(i<K){
            free(cur_centers[i]);
            free(new_centers[i]);
         }
         free(points[i]);
      }
      
   free(cur_centers);
   free(points);
   free(new_centers);
   free(distance);
   
   free(cur_group);
   free(prev_group);
   free(new_centers_cnt);
   
   yes_change = true;
   continue_looping = true;
      
   return true;
}
FILE* manage_file(char* file_name, char* type)
{
   FILE *thisfile = fopen(file_name, type);
   if (!thisfile){
      if(type[0]=='r'){
         fprintf(stderr, "ERR: couldn't read file '%s'.\n", file_name);
         return NULL;
      }
      else if (type[0]=='w'){
         fprintf(stderr, "ERR: couldn't write to file '%s'.\n", file_name);
         return NULL;
      }
      return NULL;
   }
   return thisfile;
}
bool read_in(char* file_name)
{
   int i, j;
   
   FILE* inputFile = manage_file(file_name, "r");
   if(inputFile==NULL){
      fprintf(stderr, "ERR: reading '%s' failed.\n", file_name);
      return false;
   }
   if(!fscanf(inputFile, "%i", &N)){
      fprintf(stderr, "ERR: reading N failed.\n");
      return false;
   }
   if(!fscanf(inputFile, "%i", &M)){
      fprintf(stderr, "ERR: reading M failed.\n");
      return false;
   }
   // create space for globals
   global_mallocs();
   // read from inputFile to globals
   for(i=0; i<N; i++){
      prev_group[i] = 0;
      if(i < K)
         for(j=0; j<M; j++){
            fscanf(inputFile, "%lf", &points[i][j]);
            cur_centers[i][j] = points[i][j];
         }
      else
         for(j=0; j<M; j++)
            fscanf(inputFile, "%lf", &points[i][j]);
   }
   for(i=0; i<K; i++)   // in case any centers overlap, offset them a bit
      for(j=i+1; j<K; j++)
         if(cur_centers[i][0]-cur_centers[j][0] < .01 && cur_centers[i][0]-cur_centers[j][0] > .01){
            cur_centers[j][0]+=.03;
            cur_centers[j][1]-=.03;
         }
   fclose(inputFile);
   return true;
}
void print_out(char* file_name)
{
   int i;
   // release space from globals
   FILE* outputFile = manage_file(file_name, "w");
   if (!outputFile)
      return ;
   // print N then all N final group#s 
   fprintf(outputFile, "%i\n", N);
   for(i=0; i<N; i++)
      fprintf(outputFile, "%i\n", cur_group[i]);
   // close outputFile        
   fclose(outputFile);
   global_frees();
}
void realign_groups(int start, int end, double** my_new_centers, int* my_new_centers_cnt)
{
   int i, j, k;
   double dist;
   double a_point[M];
   //a_point=(double*)malloc(M * sizeof(double));
   // set all new_centers to 0 before adding all groups to new_centers[0]
   for(i=0; i<K; i++){
      for(j=0; j<M; j++)
         my_new_centers[i][j] = 0;
      my_new_centers_cnt[i] = 0;
   }
   my_new_centers_cnt[0] = end - start;
   // every point is now assigned to center#0
   // for every point
   for(i=start; i<end; i++){
      dist=0;
      j=0;
      // for each dimension
      for(k=0; k<M; k++){
         a_point[k] = points[i][k];
         // dist = a -> b
         dist += (a_point[k] - cur_centers[j][k])*(a_point[k] - cur_centers[j][k]);
         my_new_centers[j][k] += a_point[k];
      }
      // given dist point[i] -> centers[0],
      //    record cur_group[i] = 0
      cur_group[i] = j;
      //    record distance[i] = dist
      distance[i] = dist;
      // for every other center
      for(j=1; j<K; j++){
         dist = 0;
         // for every dimension
         for(k=0; k<M; k++){
            // dist += point[i] -> center[j]
            dist += (a_point[k] - cur_centers[j][k])*(a_point[k] - cur_centers[j][k]);
         }
         // if dist < distances[i]
         if( dist < distance[i] ){
            my_new_centers_cnt[cur_group[i]]--;
            my_new_centers_cnt[j]++;
            for(k=0; k<M; k++){
               my_new_centers[cur_group[i]][k] -= a_point[k];
               my_new_centers[j][k] += a_point[k];
            }
            cur_group[i] = j;
            distance[i] = dist;
         }
      }
   }
   //free(a_point);
}
void load_local_new_centers(double **my_new_centers, int *my_new_centers_cnt)
{
   int i, j;
   // for each center
   for(i=0; i<K; i++){
      // for each dimension
      for(j=0; j<M; j++)
         new_centers[i][j] += my_new_centers[i][j];
      new_centers_cnt[i] += my_new_centers_cnt[i];
   }
}
void realign_centers(int start_index, int end_index, double** my_new_centers, int* my_new_centers_cnt)
{
   int i, j, k;
   // for each cur_centers
   for(i=start_index; i<end_index; i++){
      // cur_group[i] = j // not here, what was this about?
      // for each dimension in cur_centers
      for(j=0; j<M; j++){
         // cur_centers[i][j] = new_centers[i][j] / new_centers_cnt[i]
         // new_centers[i][j] = 0
         cur_centers[i][j] = new_centers[i][j] / new_centers_cnt[i];
         new_centers[i][j] = 0.0;
         my_new_centers[i][j] = 0.0;
      }
      // new_centers_cnt[i] = 0
      my_new_centers_cnt[i] = 0;
      new_centers_cnt[i] = 0;
   }
}
bool determine_if_continue(int start, int end)
{
   int i;
   bool myChanges = false;
   // see if any groups changed, update prev_group
   // for each point
   for(i=start; i<end; i++){
      // if prev_group[i] != cur_group[i]
      if( prev_group[i] != cur_group[i] ){
         // yes_change = true;
         // prev_group[i] = cur_group[i]
         myChanges = true;
         prev_group[i] = cur_group[i];
      }
   }
   return myChanges;
}
void* kCluster(void* input)
{
   long my_rank = (long)input;
   int i, j, k, x, y, z;
   bool this_round = true;
   double **my_new_centers = (double**)malloc(K*sizeof(double*));
   int *my_new_centers_cnt = (int*)malloc(K*sizeof(int));
   for(i=0; i<K; i++){
      my_new_centers[i] = (double*)malloc(M*sizeof(double));
      for(j=0; j<M; j++)
         my_new_centers[i][j] = 0.0;
      my_new_centers_cnt[i] = 0;
   }
   /*    Given the rank of the thread [0..P-1]
         load = N / P unless N%P!=0, then [0..N%P-1] does +1 load 
         min = rank*(N / P). Add to offset the extras in each load.
         max = min+load <= N ? min+load : N
   */
   int my_load = N / P + ( (N%P > my_rank) ? 1 : 0);
   int my_min_point = my_rank * (N / P);
   my_min_point += (N%P > my_rank) ? my_rank : N%P;
   int my_max_point = (my_min_point + my_load < N) ? my_min_point+my_load : N;
   int my_min_center = (my_rank * (K/P)) + ( (K%P>my_rank)?my_rank : K%P);
   int my_max_center = (my_min_center + (K/P+((K%P>my_rank)?1:0)) < K ) ? my_min_center + (K/P+((K%P>my_rank)?1:0)) : K;
   //printf("%li : minPoint=%2i | maxPoint=%2i | minCentr=%2i | maxCentr=%2i\n", my_rank, my_min_point, my_max_point, my_min_center, my_max_center);
   //printf("%li : load=%i | min=%i | max=%i\n", my_rank, my_load, my_min_point, my_max_point);
   z = 0;
   while( yes_change && z < MAX_TRIALS) {
      realign_groups( my_min_point, my_max_point, my_new_centers, my_new_centers_cnt);    // can't assign groups until they're all initialized (something to compare)
      // lock(mutex)
      pthread_mutex_lock(&mutex);
      load_local_new_centers(my_new_centers, my_new_centers_cnt);    // run once in each thread, but can't overlap
      // unlock(mutex)
      pthread_mutex_unlock(&mutex);
      /************BARRIER*************all points must have their group set*/
      pthread_mutex_lock(&barrier_one);
printf("%i enter barrier 1\n", (int)my_rank);
      thrd_cntr++;
      if(thrd_cntr==P){
printf("   %i unlocking barrier 1\n", (int)my_rank);
         thrd_cntr=0;
         yes_change = false;
         pthread_cond_broadcast(&cond);
      }
      else{
printf("      %i waiting barrier 1\n", (int)my_rank);
         while(pthread_cond_wait(&cond, &barrier_one) != 0);
      }
      pthread_mutex_unlock(&barrier_one);
printf("%i exit barrier 1\n", (int)my_rank);
      // threads can run this parallel, resets my_new_centers* to zeroes
      realign_centers(my_min_center, my_max_center, my_new_centers, my_new_centers_cnt);
      yes_change = determine_if_continue(my_min_point, my_max_point) ? 1 : yes_change; 
      /************BARRIER*************must know whether to continue*/
      pthread_mutex_lock(&barrier_two);
      thrd_cntr++;
      if(thrd_cntr==P){
         thrd_cntr=0;
         pthread_cond_broadcast(&cond);
      }
      else
         while(pthread_cond_wait(&cond, &barrier_two) != 0);
      pthread_mutex_unlock(&barrier_two);
      // let all determines() finish before checking yes_change
      z++;
   }
   global_rounds = z;
   free(my_new_centers_cnt);
   for(i=0; i<K; i++)
      free(my_new_centers[i]);
   free(my_new_centers);
   return NULL;
}
